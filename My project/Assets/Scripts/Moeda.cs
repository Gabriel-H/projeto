using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Moeda : MonoBehaviour
{
    public TextMeshProUGUI TextoColetavel;
    public int quantidade;

    private void OnTriggerEnter2D(Collider2D collision)
    {   
        quantidade = collision.gameObject.GetComponent<Move>().coletado++;
        TextoColetavel.text = quantidade.ToString();
        Destroy(gameObject);
    }
}
