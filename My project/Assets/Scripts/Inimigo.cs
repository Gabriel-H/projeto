using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inimigo : MonoBehaviour
{
    public Rigidbody2D rb;
    public float velocidade;
    public int Dano;
    public bool isDirectionRight;
    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
        StartCoroutine(TempoDePatrulha());
    }


    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector2(velocidade, rb.velocity.y);
        Flip();
    }

    void Flip()
    {
        if(isDirectionRight && velocidade > 0)
        {
            FlipLogic();
        }
        if(!isDirectionRight && velocidade < 0)
        {
            FlipLogic();
        }
    }

    void FlipLogic()
    {
        isDirectionRight = !isDirectionRight;
        transform.Rotate(0f, -180.0f, 0f);
    }

    IEnumerator TempoDePatrulha()
    {
        yield return new WaitForSeconds(4f);
        velocidade = velocidade * -1;
        StartCoroutine(TempoDePatrulha());
    }
}
