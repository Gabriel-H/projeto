using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bmove : MonoBehaviour
{
    public Rigidbody2D rb;
    public float velocidade;
    private GameObject jogador;
    public Vector3 posicionamento;
    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
        StartCoroutine(TempoDePatrulha());
        jogador = GameObject.FindWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector2(velocidade, rb.velocity.y);
    }

    IEnumerator TempoDePatrulha()
    {
        yield return new WaitForSeconds(4f);
        velocidade = velocidade * -1;
        StartCoroutine(TempoDePatrulha());
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {   
       jogador.transform.position = transform.position + posicionamento;
    }
}
